package com.marketplace.shoppingcartservice.service;

import com.marketplace.shoppingcartservice.entity.Client;
import com.marketplace.shoppingcartservice.exception.FailedToDeleteItemsException;
import com.marketplace.shoppingcartservice.exception.UserNotFoundException;
import com.marketplace.shoppingcartservice.ShoppingCartServiceApplication;
import com.marketplace.shoppingcartservice.repositories.CartItemRepository;
import com.marketplace.shoppingcartservice.repositories.ClientRepository;
import com.marketplace.shoppingcartservice.services.CartItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


/**
 * CartItemService Testcontainer
 */

@Testcontainers
@DirtiesContext
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"spring.liquibase.enabled=true"})
@ContextConfiguration(classes = ShoppingCartServiceApplication.class)
public class CartItemServiceTest {

    @Container
    public static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:13.2")
            .withDatabaseName("shopping_cart")
            .withPassword("admin")
            .withUsername("postgres");

    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
    }

    @Autowired
    CartItemService cartItemService;
    @MockBean
    private CartItemRepository cartItemRepository;

    @MockBean
    private ClientRepository clientRepository;
    private UUID clientId;
    private UUID wrongClientId;
    private UUID clientIdForDeleteExceptionCase;
    private UUID itemId;
    private List<UUID> itemIds;
    private Client clientForDeleteExceptionCase;
    private Client client;

    @BeforeEach
    public void setup() {
        clientId = UUID.randomUUID();
        wrongClientId = UUID.randomUUID();
        clientIdForDeleteExceptionCase = UUID.randomUUID();
        itemId = UUID.randomUUID();
        itemIds = new ArrayList<>();
        itemIds.add(itemId);
        client = new Client(clientId, "Bob");
        clientForDeleteExceptionCase = new Client(clientIdForDeleteExceptionCase,"Bug");
        //for correct execution
        when(clientRepository.findById(clientId)).thenReturn(Optional.of(client));
        when(cartItemRepository.existsByClientAndItemId(client, itemIds)).thenReturn(false);
        //for UserNotFoundException case
        when(clientRepository.findById(wrongClientId)).thenReturn(Optional.empty());
        //for FailedToDeleteItemsException case
        when(clientRepository.findById(clientIdForDeleteExceptionCase)).thenReturn(Optional.of(clientForDeleteExceptionCase));
        when(cartItemRepository.existsByClientAndItemId(clientForDeleteExceptionCase, itemIds)).thenReturn(true);
    }

    @Test
    void deleteItemsByIdAndItemIdsShouldBeExecutedCorrectly(){
        assertAll(()->cartItemService.deleteItemsByIdAndItemIds(clientId, itemIds));
    }

    @Test
    void deleteItemsByIdAndItemIdsShouldThrowUserNotFoundException(){
        assertThrows(UserNotFoundException.class,() -> cartItemService
                .deleteItemsByIdAndItemIds(wrongClientId, itemIds),
                "UserNotFoundException expected to be thrown, but it didn't");
    }

    @Test
    void deleteItemsByIdAndItemIdsShouldThrowFailedToDeleteItemsException(){
        assertThrows(FailedToDeleteItemsException.class,() -> cartItemService
                        .deleteItemsByIdAndItemIds(clientIdForDeleteExceptionCase, itemIds),
                "FailedToDeleteItemsException expected to be thrown, but it didn't");
    }
}
