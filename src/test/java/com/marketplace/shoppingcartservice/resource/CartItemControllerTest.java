package com.marketplace.shoppingcartservice.resource;

import com.marketplace.shoppingcartservice.ShoppingCartServiceApplication;
import com.marketplace.shoppingcartservice.exception.FailedToDeleteItemsException;
import com.marketplace.shoppingcartservice.exception.UserNotFoundException;
import com.marketplace.shoppingcartservice.services.CartItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * CartItemController Testcontainer
 */

@Testcontainers
@DirtiesContext
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"spring.liquibase.enabled=true"})
@ContextConfiguration(classes = ShoppingCartServiceApplication.class)
public class CartItemControllerTest {
    @Container
    public static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:13.2")
            .withDatabaseName("shopping_cart")
            .withPassword("admin")
            .withUsername("postgres");

    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
    }

    @Autowired
    private MockMvc mvc;
    @MockBean
    private CartItemService cartItemService;
    private UUID clientId;
    private UUID wrongClientId;
    private UUID idForFailedToDeleteException;
    private UUID itemId1;
    private UUID itemId2;

    @BeforeEach
    public void setup() {
        clientId = UUID.randomUUID();
        wrongClientId = UUID.randomUUID();
        idForFailedToDeleteException = UUID.randomUUID();
        itemId1 = UUID.randomUUID();
        itemId2 = UUID.randomUUID();
        List<UUID> itemIds = new ArrayList<>();
        itemIds.add(itemId1);
        itemIds.add(itemId2);
        doNothing().when(cartItemService).deleteItemsByIdAndItemIds(clientId, itemIds);
        doThrow(new UserNotFoundException(wrongClientId)).when(cartItemService)
                .deleteItemsByIdAndItemIds(wrongClientId, itemIds);
        doThrow(new FailedToDeleteItemsException(idForFailedToDeleteException, itemIds))
                .when(cartItemService).deleteItemsByIdAndItemIds(idForFailedToDeleteException, itemIds);
    }

    @Test
    void deleteCartItemsTestShouldReturnOk() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/shopping-cart/" + clientId + "/delete?itemId=" + itemId1 + "," + itemId2))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteCartItemsTestExpectFailedToDeleteException() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/shopping-cart/" + idForFailedToDeleteException + "/delete?itemId=" + itemId1 + "," + itemId2))
                .andDo(print())
                .andExpect(status().isInternalServerError());
    }

    @Test
    void deleteCartItemsTestExpectUserNotFoundException() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/shopping-cart/" + wrongClientId + "/delete?itemId=" + itemId1 + "," + itemId2))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
