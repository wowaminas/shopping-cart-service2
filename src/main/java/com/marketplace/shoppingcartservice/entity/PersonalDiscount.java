package com.marketplace.shoppingcartservice.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//персональная скидка для пользователя

@Entity
@Data
@NoArgsConstructor
@Table(name = "personal_discount", schema = "shopping_cart")
public class PersonalDiscount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;    //пользователь

    @Column(name = "discount_percent")
    private Integer discountPercent;   //размер персональной скидки клиента в процентах

}
