package com.marketplace.shoppingcartservice.resource;

import com.marketplace.shoppingcartservice.api.resource.OrdersResource;
import com.marketplace.shoppingcartservice.services.CartItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


import java.util.UUID;


@AllArgsConstructor
@Slf4j
@RestController
public class OrdersController implements OrdersResource {

    private final CartItemService cartItemService;


    @Override
    public ResponseEntity<String> createOrderByIdClient(UUID id) {
        log.info("Получен запрос на создание заказа по id клиента: {}", id);
        return ResponseEntity.ok(cartItemService.orderFormationByClientId(id));
    }
}
