package com.marketplace.shoppingcartservice.resource;

import com.marketplace.shoppingcartservice.api.resource.CartItemResource;
import com.marketplace.shoppingcartservice.dto.CartDto;
import com.marketplace.shoppingcartservice.entity.CartItem;
import com.marketplace.shoppingcartservice.services.CartItemService;
import lombok.AllArgsConstructor;
import com.marketplace.shoppingcartservice.dto.CartItemDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
@RestController
public class CartItemController implements CartItemResource {

    private final CartItemService cartItemService;


    /**
     * Контроллер возвращает все товары в корзине с общей полной ценой и общей ценой со скидкой(если она есть)
     */
    @Override
    public ResponseEntity<CartDto> showAllCartItems(UUID id) {
        return ResponseEntity.ok(cartItemService.getCartDto(id));
    }


    @Override
    public ResponseEntity<CartItem> addCartItem(CartItemDto cartItemDto, UUID id) {
        return ResponseEntity.ok(cartItemService.addCartItem(cartItemDto, id));
    }

    /**
     * Delete контроллер удаляет товары из корзины клиента по profile_id и списку item_id.
     * в виде : localhost:8080/cart/{id}/delete?itemId=firstValue,secondValue,thirdValue.
     * PathVariable id в данном случае является client_id таблицы Cart_item
     */
    @Override
    public ResponseEntity<HttpStatus> deleteCartItems(UUID id, List<UUID> itemId) {
        log.info("Received a request to delete an item with id {} from a customer with id {}", itemId, id);
        cartItemService.deleteItemsByIdAndItemIds(id, itemId);
        log.info("Items with id {} have been successfully deleted from the customer's shopping cart with id {}",
                itemId, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
