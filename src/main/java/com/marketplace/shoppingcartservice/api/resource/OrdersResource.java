package com.marketplace.shoppingcartservice.api.resource;

import com.marketplace.shoppingcartservice.dto.CartItemMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.UUID;

@Tags(
        value = {
                @Tag(name = "Заказ", description = "Заказ клиента")
        }
)
@RequestMapping("/cart")
public interface OrdersResource {

    @PostMapping("/order/{id}")
    @Operation(summary = "Формирование заказа по id клиента")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Заказ клиента сформирован",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CartItemMessage.class)) }),
            @ApiResponse(responseCode = "400", description = "Предоставлен неверный идентификатор",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Не удалось сформировать заказ по заданному идентификатору",
                    content = @Content)})
        ResponseEntity<String> createOrderByIdClient(@PathVariable UUID id);
}
