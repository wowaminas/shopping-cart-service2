package com.marketplace.shoppingcartservice.util;

import com.marketplace.shoppingcartservice.dto.CartItemDto;
import com.marketplace.shoppingcartservice.entity.CartItem;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CartItemMapper {

    private final ModelMapper modelMapper;

    public CartItemDto convertItemToDto (CartItem cartItem) {

        return modelMapper.map(cartItem, CartItemDto.class);
    }

    public CartItem convertDtoToItem(CartItemDto cartItemDto) {

        return modelMapper.map(cartItemDto, CartItem.class);
    }
}
