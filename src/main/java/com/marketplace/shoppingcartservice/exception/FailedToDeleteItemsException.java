package com.marketplace.shoppingcartservice.exception;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

@Slf4j
public class FailedToDeleteItemsException extends RuntimeException{
    public FailedToDeleteItemsException(UUID profileId, List<UUID> itemId) {
        super("Failed to delete items with id " + itemId + " from the customer's shopping cart with id " + profileId);
    }
}
