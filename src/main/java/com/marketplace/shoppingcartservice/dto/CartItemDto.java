package com.marketplace.shoppingcartservice.dto;

import com.marketplace.shoppingcartservice.entity.Client;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CartItemDto {
    @Schema(example = "1", description = "Уникальный идентификатор корзины")
    @NotNull(message = "account.dto.validate.accountNumber.isEmpty")
    private Long id;

    @Schema(example = "123e4567-e89b-12d3-a456-426614174000", description = "Уникальный идентификатор товара")
    @NotNull(message = "account.dto.validate.accountNumber.isEmpty")
    private UUID itemId;

    @Schema(example = "Профиль пользователя", description = "Уникальный профиль пользователя")
    @NotNull(message = "account.dto.validate.accountNumber.isEmpty")
    private Client client;

    @NotNull(message = "account.dto.validate.accountNumber.isEmpty")
    @Schema(example = "1", description = "Количество продукта")
    private Integer count;

    @NotNull(message = "account.dto.validate.accountNumber.isEmpty")
    @Schema(example = "5000", description = "Цена продукта")
    private Long price;

    @NotNull(message = "account.dto.validate.accountNumber.isEmpty")
    @Schema(example = "да", description = "Наличие скидки на продукт")
    private Boolean isDiscounted;

    @Schema(example = "4000", description = "Цена продукта с учетом скидки")
    private Long discountPrice;


}
